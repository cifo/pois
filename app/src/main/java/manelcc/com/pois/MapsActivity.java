package manelcc.com.pois;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.OnStreetViewPanoramaReadyCallback;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.StreetViewPanoramaFragment;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import manelcc.com.pois.bd.MySqlDTO;
import manelcc.com.pois.utils.Constants;
import manelcc.com.pois.utils.Poi;
import manelcc.com.pois.utils.Utils;


public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, OnStreetViewPanoramaReadyCallback {

    private MySqlDTO db;
    private int id;
    private Poi detail;
    private LatLng position;
    private FloatingActionButton fab;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
        setContentView(R.layout.activity_maps);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        //Obtener la consulta de la Bd
        detail = db.getDetailPoi(id);

        //actualiza elementos
        updateElements();

        //Inicia GoogleMaps y StreetView
        initMapsStreet();

        //FAB PARA COMPARTIR
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(android.content.Intent.ACTION_SEND);
                intent.setType("*/*");
                intent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.sharelocation));
                intent.putExtra(Intent.EXTRA_TEXT, String.format(getString(R.string.inf), detail.getTitle(), detail.getAdress()));
                startActivity(Intent.createChooser(intent, "VISITA ESTE LUGAR"));
            }
        });
    }

    private void initMapsStreet() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // Obtain the StreetView and get notified when the map is ready to be used.
        StreetViewPanoramaFragment streetViewPanoramaFragment =
                (StreetViewPanoramaFragment) getFragmentManager()
                        .findFragmentById(R.id.streetviewpanorama);
        streetViewPanoramaFragment.getStreetViewPanoramaAsync(this);

    }

    private void updateElements() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(detail.getTitle());
            getSupportActionBar().setSubtitle(detail.getUrl());

        }
        position = new LatLng(getStringSplit(Constants.LATITUD), getStringSplit(Constants.LONGITUDE));
    }

    private void init() {
        id = getIntent().getExtras().getInt("position");
        db = new MySqlDTO(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        GoogleMap mMap = googleMap;
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                // Getting view from the layout file info_window_layout
                View v = getLayoutInflater().inflate(R.layout.infowindow, null);

                // Getting references
                TextView title = (TextView) v.findViewById(R.id.title);
                title.setText(detail.getTitle());
                TextView adress = (TextView) v.findViewById(R.id.adress);
                if (detail.getAdress().equals("null")) {
                    adress.setVisibility(View.GONE);
                } else {
                    adress.setText(detail.getAdress());
                }
                TextView description = (TextView) v.findViewById(R.id.description);
                description.setText(detail.getDescription());
                TextView transport = (TextView) v.findViewById(R.id.transport);
                if (detail.getTransport().equals("null")) {
                    transport.setVisibility(View.GONE);
                } else {
                    transport.setText(detail.getTransport());

                }

                return v;
            }
        });
        mMap.addMarker(new MarkerOptions().position(position));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 18));
    }

    @Override
    public void onStreetViewPanoramaReady(StreetViewPanorama panorama) {
        panorama.setPosition(position);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_maps, menu);

        MenuItem call = menu.findItem(R.id.email);

        MenuItem informacion = menu.findItem(R.id.url);

        if (detail.getUrl().equals("null")) {

            informacion.setVisible(false);

        } else if (detail.getUrl().startsWith("http")) {

            informacion.setVisible(true);

        } else if (detail.getUrl().startsWith("Tel")) {

            informacion.setIcon(R.drawable.ic_call_white_48dp);

        } else if (detail.getUrl().contains("@")) {

            informacion.setIcon(R.drawable.ic_email_white_48dp);

        }


        if (detail.getEmail().startsWith("Tel")) {

            call.setIcon(R.drawable.ic_call_white_48dp);

        } else if (detail.getEmail().contains("@")) {

            call.setIcon(R.drawable.ic_email_white_48dp);

        } else {

            call.setVisible(false);
        }


        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.email:

                if (detail.getEmail().startsWith("Tel")) {

                    sendCall(detail.getEmail());

                } else if (detail.getEmail().contains("@")) {

                    sendEmail();
                }

                break;

            case R.id.url:

                if (detail.getUrl().startsWith("http")) {
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(detail.getUrl())));
                    } catch (ActivityNotFoundException e) {
                        Utils.showSnack(fab, getString(R.string.infMalformation));
                    }
                } else if (detail.getUrl().equals("null")) {

                    break;

                } else if (detail.getUrl().startsWith("Tel")) {

                    sendCall(detail.getUrl());

                } else if (detail.getUrl().contains("@")) {

                    sendEmail();
                }

                break;

            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void sendCall(String phoneNumber) {

        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phoneNumber.substring(5)));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }


    private void sendEmail() {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", detail.getEmail(), null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.pedirInf));
        startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }

    /**
     * obtain position
     *
     * @return doubles
     */
    public Double getStringSplit(String location) {

        String[] position = detail.getGeocoordinates().split(",");

        if (location.equals(Constants.LATITUD)) {
            return Double.parseDouble(position[0]);
        } else if (location.equals(Constants.LONGITUDE)) {
            return Double.parseDouble(position[1]);
        } else
            return null;
    }
}
