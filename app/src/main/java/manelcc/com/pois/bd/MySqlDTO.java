package manelcc.com.pois.bd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import manelcc.com.pois.utils.Constants;
import manelcc.com.pois.utils.Poi;


/**
 * Created by manelcc on 07/11/2015.
 */
public class MySqlDTO {

    private final MySqlDAO dao;

    public MySqlDTO(Context context) {
        //CONSTRUCTOR EL CUAL INSTANCIA AL DAO
        dao = new MySqlDAO(context);
    }


    public Cursor getAllList() {

        String query = ("SELECT * FROM " + Constants.TABLE_LIST);
        SQLiteDatabase db = dao.getReadableDatabase();

        return db.rawQuery(query, null);


    }

    public void insertList(JSONObject response) {
        try {
            SQLiteDatabase db = dao.getWritableDatabase();

            JSONArray list = response.getJSONArray("list");

            for (int i = 0; i < list.length(); i++) {
                ContentValues values = new ContentValues();
                //title
                values.put(Constants.Colums.FIELD_TITLE, list.getJSONObject(i).getString(Constants.Colums.FIELD_TITLE));

                //location
                values.put(Constants.Colums.FIELD_COORDINATES, list.getJSONObject(i).getString(Constants.Colums.FIELD_COORDINATES));


                db.insert(Constants.TABLE_LIST, null, values);
            }
            db.close();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void insertDetail(JSONObject response, int id) {
        try {
            SQLiteDatabase db = dao.getWritableDatabase();
            ContentValues values = new ContentValues();

            //2 adress
            values.put(Constants.Colums.FIELD_ADRESS, response.getString(Constants.Colums.FIELD_ADRESS));


            //3 transport
            values.put(Constants.Colums.FIELD_TRANSPORT, response.getString(Constants.Colums.FIELD_TRANSPORT));
            //4 url
            values.put(Constants.Colums.FIELD_URL, response.getString(Constants.Colums.FIELD_URL));

            //6 description
            values.put(Constants.Colums.FIELD_DESCRIPTION, response.getString(Constants.Colums.FIELD_DESCRIPTION));
            //7 email
            values.put(Constants.Colums.FIELD_EMAIL, response.getString(Constants.Colums.FIELD_EMAIL));

            //update
            db.update(Constants.TABLE_LIST, values, Constants.Colums.FIELD_ID + " = ?", new String[]{String.valueOf(id)});

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public Poi getDetailPoi(int id) {
        Poi detail = null;
        SQLiteDatabase db = dao.getReadableDatabase();
        String query = ("SELECT * FROM "
                + Constants.TABLE_LIST
                + " WHERE "
                + Constants.Colums.FIELD_ID
                + " = "
                + id + ";");
        Cursor result = db.rawQuery(query, null);
        if (result != null) {
            result.moveToFirst();
            detail = new Poi();
            detail.setTitle(result.getString(1));
            detail.setAdress(result.getString(2));
            detail.setTransport(result.getString(3));
            detail.setUrl(result.getString(4));
            detail.setGeocoordinates(result.getString(5));
            detail.setDescription(result.getString(6));
            detail.setEmail(result.getString(7));
        }
        return detail;
    }
}
