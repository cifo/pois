package manelcc.com.pois.bd;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import manelcc.com.pois.utils.Constants;


/**
 * Created by manelcc on 07/11/2015.
 */
public class MySqlDAO extends SQLiteOpenHelper {

    public MySqlDAO(Context context) {
        super(context, Constants.DATABASE_NAME, null, Constants.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_TABLE =
                "CREATE TABLE " + Constants.TABLE_LIST + " ( "
                        + Constants.Colums.FIELD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                        + Constants.Colums.FIELD_TITLE + " TEXT, "
                        + Constants.Colums.FIELD_ADRESS + " TEXT, "
                        + Constants.Colums.FIELD_TRANSPORT + " TEXT, "
                        + Constants.Colums.FIELD_URL + " TEXT, "
                        + Constants.Colums.FIELD_COORDINATES + " TEXT, "
                        + Constants.Colums.FIELD_DESCRIPTION + " TEXT, "
                        + Constants.Colums.FIELD_EMAIL + " TEXT )";


        try {
            db.execSQL(CREATE_TABLE);
        } catch (SQLException ignored) {

        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
