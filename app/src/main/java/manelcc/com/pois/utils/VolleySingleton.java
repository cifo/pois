package manelcc.com.pois.utils;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by manelcc on 08/11/2015.
 * Singleton para obtener trabajar una única instancia.
 */
public final class VolleySingleton {

    private static VolleySingleton singleton;
    private RequestQueue requestQueue;
    private static Context context;

    /**
     * Constructor
     *
     * @param context
     */
    private VolleySingleton(Context context) {
        VolleySingleton.context = context;
        requestQueue = getRequestQueue();
    }

    /**
     * Solicitud de Instancia. Si existe devolver la existente
     * en caso contrario crear una nueva instancia.
     *
     * @param context
     * @return
     */
    public static synchronized VolleySingleton getInstance(Context context) {
        if (singleton == null) {

            singleton = new VolleySingleton(context);
        }

        return singleton;
    }

    /**
     * obtener la cola de peticiones
     *
     * @return
     */
    public RequestQueue getRequestQueue() {
        if (requestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            requestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return requestQueue;
    }

    /**
     * Para agregar a la cola de peticiones, las nuevas
     *
     * @param req
     * @param <T>
     */
    public <T> void addToRequestQueue(Request<T> req) {

        getRequestQueue().add(req);
    }


}
