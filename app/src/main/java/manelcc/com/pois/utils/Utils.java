package manelcc.com.pois.utils;

import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

/**
 * Created by manelcc on 07/11/2015.
 */
public class Utils {

    /**
     * Shows a SnackBar
     */

    public static void showSnack(View v, String text) {

        Snackbar snack = Snackbar.make(v, text, Snackbar.LENGTH_LONG);
        View snackView = snack.getView();
        TextView textView = (TextView) snackView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        //snackView.setBackgroundResource(R.color.txt_colorR197G64B21);

        snack.show();
    }

    /**
     * Called verify Network is avaible
     *
     * @param context
     * @return
     */
    public static boolean isNetworkReachable(Context context) {
        final ConnectivityManager mManager =
                (ConnectivityManager) context.getSystemService(
                        Context.CONNECTIVITY_SERVICE);
        NetworkInfo current = mManager.getActiveNetworkInfo();
        return current != null && (current.getState() == NetworkInfo.State.CONNECTED);
    }


}
