package manelcc.com.pois.utils;

/**
 * Created by manelcc on 07/11/2015.
 */
public class Constants {


    //Version Database
    public static final int DATABASE_VERSION = 1;

    // Name Database
    public static final String DATABASE_NAME = "PoisDB";

    public static final String TABLE_LIST = "list";


    //Url services for get a List
    public static final String URL_LIST = "http://t21services.herokuapp.com/points";
    public static final String URL_LIST_DETAIL = "http://t21services.herokuapp.com/points/";
    public static final String LATITUD = "latitud";
    public static final String LONGITUDE = "longitude";

    // CONSTANS MAP DATABASE
    public static class Colums {
        public static final String FIELD_ID = "_id";
        public static final String FIELD_TITLE = "title";
        public static final String FIELD_ADRESS = "address";
        public static final String FIELD_TRANSPORT = "transport";
        public static final String FIELD_URL = "email";
        public static final String FIELD_COORDINATES = "geocoordinates";
        public static final String FIELD_DESCRIPTION = "description";
        public static final String FIELD_EMAIL = "phone";
    }


}
