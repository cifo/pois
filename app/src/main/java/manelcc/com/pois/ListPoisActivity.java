package manelcc.com.pois;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import manelcc.com.pois.bd.MySqlDTO;
import manelcc.com.pois.utils.Constants;
import manelcc.com.pois.utils.Utils;
import manelcc.com.pois.utils.VolleySingleton;


public class ListPoisActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {


    private MySqlDTO db;
    private ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        super.onCreate(savedInstanceState);

        //Call BD
        db = new MySqlDTO(this);
        setContentView(R.layout.activity_list_pois);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Referenciar ListView
        lv = (ListView) findViewById(R.id.listView);

        //Comprobar si bd tiene datos, en caso contrario inserta datos en Bd y mostrar luego lv
        isBdempty(simpleCursorAdapter().isEmpty());
        lv.setOnItemClickListener(this);

    }


    /**
     * Call for show lv from BD o Volley
     *
     * @param empty
     */
    private void isBdempty(boolean empty) {

        if (empty) {

            if (Utils.isNetworkReachable(this)) {
                requestCallVolley();
                this.setProgressBarIndeterminate(true);
            } else {
                Utils.showSnack(lv, getString(R.string.infNoConection));
            }

        } else {
            this.setProgressBarIndeterminateVisibility(false);
            lv.setAdapter(simpleCursorAdapter());
        }
    }


    /**
     * Call for request
     */
    private void requestCallVolley() {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Constants.URL_LIST,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        db.insertList(response);
                        isBdempty(false);
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {

                        ListPoisActivity.this.setProgressBarIndeterminateVisibility(false);
                        Snackbar.make(lv, getString(R.string.errorVolley), Snackbar.LENGTH_LONG).show();
                    }
                });
        VolleySingleton.getInstance(ListPoisActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    /**
     * SimpleCursorAdapter for show a list Pois
     *
     * @return adapter
     */
    private SimpleCursorAdapter simpleCursorAdapter() {

        return new SimpleCursorAdapter(this
                , android.R.layout.simple_list_item_1
                , db.getAllList()
                , new String[]{Constants.Colums.FIELD_TITLE}
                , new int[]{android.R.id.text1}
                , 1);
    }


    /**
     * Call Interface from item lv
     *
     * @param parent
     * @param view
     * @param position
     * @param id
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        int i = position + 1;
        if (db.getDetailPoi(i).getAdress() != null) {
            goMapsActivity(i);

        } else {
            requestDetailVolley(i);
        }
    }

    private void requestDetailVolley(final int id) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Constants.URL_LIST_DETAIL + String.valueOf(id),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        db.insertDetail(response, id);
                        goMapsActivity(id);

                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ListPoisActivity.this.setProgressBarIndeterminateVisibility(false);
                        Snackbar.make(lv, getString(R.string.errorVolley), Snackbar.LENGTH_LONG).show();
                    }
                });
        VolleySingleton.getInstance(ListPoisActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    private void goMapsActivity(int id) {
        startActivity(new Intent(ListPoisActivity.this, MapsActivity.class).putExtra("position", id)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);

    }
}
